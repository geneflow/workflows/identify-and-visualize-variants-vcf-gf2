# identify-and-visualize-variants-vcf-gf2 geneflow2 workflow

A geneflow v2 workflow that maps sequencing data, identifies variants, and creates visualizatons using vcfR

Inputs
------
1. Forward read file (FASTQ)
2. Reverse read file (FASTQ)
3. Reference file (FASTA)

Outputs
-------
1. a PDF file that contains the generated visualization graph

Apps Documentation
------------------
Documentation for the individual apps are in their respective directories:

|    https://gitlab.com/geneflow/apps/bwa-index-gf2.git

|    https://gitlab.com/geneflow/apps/bwa-mem-gf2.git

|    https://gitlab.com/geneflow/apps/samtools-sam-to-bam-gf2.git

|    https://gitlab.com/geneflow/apps/bam-sort-gf2.git

|    https://gitlab.com/geneflow/apps/freebayes-gf2.git

|    https://gitlab.com/geneflow/apps/vcfr-gf2.git
